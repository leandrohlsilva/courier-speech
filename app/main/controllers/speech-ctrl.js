'use strict';
angular.module('main')
  .controller('SpeechCtrl', ['$scope', '$log', 'Main', 'TTS', function ($scope, $log, Main, TTS) {

    $log.log('Hello from your Controller: DebugCtrl in module main:. This is your controller:', this);

    // bind data from services
    $scope.speechInstructions = Main.speechInstructions;

    ionic.Platform.ready(function () {
      $scope.recognition = new SpeechRecognition();
      $scope.recognition.onresult = function (event) {
        $scope.$apply(function () {
          if (event.results.length > 0) {
            $scope.results = event.results[0][0].transcript;
            var words = $scope.results.split(' ');
            var foundInstruction = false;
            for (var i in words) {
              if ($scope.speechInstructions[words[i]]) {
                $scope.$broadcast('SPEECH_INSTRUCTION_FOUND', $scope.speechInstructions[words[i]]);
                foundInstruction = true;
              }
            }
            if (!foundInstruction) {
              $scope.$broadcast('SPEECH_INSTRUCTION_NOT_FOUND', $scope.results);
            }
          }
        });
      };

      function onError () {
        $scope.$broadcast('SPEECH_INSTRUCTION_ERROR', $scope.results);
      }
      $scope.recognition.onerror = onError;
      $scope.recognition.onnomatch = onError;

    }.bind(this));

    $scope.receiveJob = function () {
      if (Main.jobExamples.list.length > 0) {
        if (!$scope.currentJob) {
          $scope.currentJob = Main.jobExamples.list.pop();
          $scope.presentJob($scope.currentJob);
        }
      } else {
        TTS.speak('No more jobs available at the moment');
      }
    };

    $scope.presentJob = function (job) {
      TTS.speak(Main.jobExamples.jobIncomingMessage + '. ' + job, function () {
        $scope.recognition.start();
      });
    };

    $scope.$on('SPEECH_INSTRUCTION_FOUND', function (scope, instruction) {
      TTS.speak(instruction.response, function () {
        $scope.currentJob = null;
      });
    });
    $scope.$on('SPEECH_INSTRUCTION_NOT_FOUND', function (scope, transcript) {
      TTS.speak('I\'m sorry, I didn\'t get that. I hear ' + transcript, function () {
        $scope.presentJob($scope.currentJob);
      });
    });

    $scope.$on('SPEECH_INSTRUCTION_ERROR', function () {
      TTS.speak('I\'m sorry, I didn\'t get that.', function () {
        $scope.presentJob($scope.currentJob);
      });
    });
  }]);
