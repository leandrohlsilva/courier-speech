'use strict';
angular.module('main')
.service('Main', function ($log, $timeout) {

  $log.log('Hello from your Service: Main in module main');

  // some initial data
  this.someData = {
    binding: 'Yes! Got that databinding working'
  };

  this.speechInstructions = {
    'accept': {id: 1, description: 'Accept a job', response: 'You have accepted this job'},
    'reject': {id: 2, description: 'Reject a job', response: 'You have reject this job'},
    'repeat': {id: 3, description: 'Repeat message', response: 'Repeating...'},
    'more': {id: 4, description: 'More details', response: 'More details'},
    'stop': {id: 5, description: 'Stop recording', response: 'You\'ve chosen to stop'}
  };

  this.jobExamples = {
    jobIncomingMessage: 'Job incoming: ',
    list: [
      'Deliver letter from Gophr on 9 White Lion Street to iSmash on 22 Church Street in 1 hour and 30 minutes',
      'Deliver cellphone from Lovefone on 37 Tottenham Street to Leandro on 17-19 Delancey Street anytime'
    ]
  };

  this.changeBriefly = function () {
    var initialValue = this.someData.binding;
    this.someData.binding = 'Yeah this was changed';

    var that = this;
    $timeout(function () {
      that.someData.binding = initialValue;
    }, 500);
  };

});
