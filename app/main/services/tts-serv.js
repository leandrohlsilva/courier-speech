'use strict';
angular.module('main')
  .service('TTS', [function () {

    this.speak = function (speech, callback, callbackError) {
      callback = callback || angular.noop;
      function defaultCallbackError (reason) {
        alert(reason);
      }

      callbackError = callbackError || defaultCallbackError;

      TTS.speak({
        text: speech,
        locale: 'en-GB',
        rate: 0.75
      }, callback, callbackError);
    };
  }]);
